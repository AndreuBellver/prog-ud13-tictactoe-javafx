package com.bellver.prog;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainApp extends Application {

    boolean sw = true;
    Button unoButton = new Button();
    Button dosButton = new Button();
    Button tresButton = new Button();
    Button cuatroButton = new Button();
    Button cincoButton = new Button();
    Button seisButton = new Button();
    Button sieteButton = new Button();
    Button ochoButton = new Button();
    Button nueveButton = new Button();

    Button exit = new Button("Exit");
    Button reset = new Button("Reset");

    Stage secondaryStage = new Stage();



    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        VBox main = new VBox(20);
        int TamanyBoto = 100;

        primaryStage.setTitle("Tic Tac Toe");

        GridPane grid = new GridPane();
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(20, 20, 20, 20));

        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                secondaryStage.close();
                primaryStage.close();

            }
        });

        reset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                secondaryStage.close();
                primaryStage.close();
                clearTable();
                primaryStage.show();

            }
        });

        unoButton.setMinWidth(TamanyBoto);
        unoButton.setMinHeight(TamanyBoto);
        unoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(unoButton);

            }
        });

        dosButton.setMinWidth(TamanyBoto);
        dosButton.setMinHeight(TamanyBoto);
        dosButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(dosButton);

            }
        });

        tresButton.setMinWidth(TamanyBoto);
        tresButton.setMinHeight(TamanyBoto);
        tresButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(tresButton);
            }
        });

        cuatroButton.setMinWidth(TamanyBoto);
        cuatroButton.setMinHeight(TamanyBoto);
        cuatroButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(cuatroButton);
            }
        });

        cincoButton.setMinWidth(TamanyBoto);
        cincoButton.setMinHeight(TamanyBoto);
        cincoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(cincoButton);
            }
        });

        seisButton.setMinWidth(TamanyBoto);
        seisButton.setMinHeight(TamanyBoto);
        seisButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(seisButton);
            }
        });

        sieteButton.setMinWidth(TamanyBoto);
        sieteButton.setMinHeight(TamanyBoto);
        sieteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(sieteButton);
            }
        });

        ochoButton.setMinWidth(TamanyBoto);
        ochoButton.setMinHeight(TamanyBoto);
        ochoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(ochoButton);
            }
        });

        nueveButton.setMinWidth(TamanyBoto);
        nueveButton.setMinHeight(TamanyBoto);
        nueveButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                colocarMarca(nueveButton);
            }
        });

        grid.add(unoButton, 0, 0);
        grid.add(dosButton, 0, 1);
        grid.add(tresButton, 0, 2);
        grid.add(cuatroButton, 1, 0);
        grid.add(cincoButton, 1, 1);
        grid.add(seisButton, 1, 2);
        grid.add(sieteButton, 2, 0);
        grid.add(ochoButton, 2, 1);
        grid.add(nueveButton, 2, 2);

        Scene escena = new Scene(main, 400, 450);

        Label gameName = new Label(" Tic - Tac - Toe ");


        main.getChildren().addAll(gameName, grid, exit);
        main.setAlignment(Pos.CENTER);

        primaryStage.setScene(escena);
        primaryStage.show();
    }

    private void clearTable(){

         unoButton.setText("");
         dosButton.setText("");
         tresButton.setText("");
         cuatroButton.setText("");
         cincoButton.setText("");
         seisButton.setText("");
         sieteButton.setText("");
         ochoButton.setText("");
         nueveButton.setText("");
    }

    private void colocarMarca(Button boton) {


        String contenido = boton.getText();
        if(contenido.length()==0) {
            boton.setText(sw ? "X" : "O");
            sw = !sw;

            if (isTicTacToe()){

                VBox winnerMessage = new VBox(new Label("HAS GANADO!!!!"),exit,reset);
                winnerMessage.setSpacing(10);
                winnerMessage.setAlignment(Pos.CENTER);

                Scene winner = new Scene(winnerMessage,200,225);

                secondaryStage.setScene(winner);
                secondaryStage.show();

            } else if (isTie()){

                VBox tieMessage = new VBox(new Label("EMPATE!!"),exit,reset);
                tieMessage.setSpacing(10);
                tieMessage.setAlignment(Pos.CENTER);

                Scene tie = new Scene(tieMessage,200,225);

                secondaryStage.setScene(tie);
                secondaryStage.show();

            }

        }

    }

    private boolean isTie(){
        if (unoButton.getText().length() != 0 &&  dosButton.getText().length() != 0 &&
                tresButton.getText().length() != 0 && cuatroButton.getText().length() != 0 &&
                cincoButton.getText().length() != 0 && seisButton.getText().length() != 0 &&
                sieteButton.getText().length() != 0 && ochoButton.getText().length() != 0 &&
                nueveButton.getText().length() != 0){

            return true;
        }

        return false;
    }

    private boolean isTicTacToe() {

        int ganar = 0;
        String opcion1 = unoButton.getText() + dosButton.getText() + tresButton.getText();
        String opcion2 = cuatroButton.getText() + cincoButton.getText() + seisButton.getText();
        String opcion3 = sieteButton.getText() + ochoButton.getText() + nueveButton.getText();
        String opcion4 = unoButton.getText() + cuatroButton.getText() + sieteButton.getText();
        String opcion5 = dosButton.getText() + cincoButton.getText() + ochoButton.getText();
        String opcion6 = tresButton.getText() + seisButton.getText() + nueveButton.getText();
        String opcion7 = unoButton.getText() + cincoButton.getText() + nueveButton.getText();
        String opcion8 = sieteButton.getText() + cincoButton.getText() + tresButton.getText();
        String X = "XXX";
        String O = "OOO";

        if (opcion1.equals(X) || opcion2.equals(X) || opcion3.equals(X) || opcion4.equals(X) || opcion5.equals(X)
                || opcion6.equals(X) || opcion7.equals(X) || opcion8.equals(X)) {
            ganar++;
        } else if (opcion1.equals(O) || opcion2.equals(O) || opcion3.equals(O) || opcion4.equals(O) || opcion5.equals(O)
                || opcion6.equals(O) || opcion7.equals(O) || opcion8.equals(O)) {
            ganar++;
        }
        if (ganar == 1) {
            return true;
        } else {
            return false;
        }
    }

}